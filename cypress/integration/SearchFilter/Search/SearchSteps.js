import {Given, When, Then, And} from "cypress-cucumber-preprocessor/steps";
import {HomePage} from "../../Pages/HomePage";
import {AllCarsPage} from "../../Pages/AllCarsPage";

When('user select Brand and Model of car and click on search button', () => {
    HomePage.brandNameDiv.click()
    HomePage.brandListDiv.any().click()
    HomePage.modalNameDiv.click()
    HomePage.modalListDiv.any().click()
    HomePage.searchBtn.click()
})

Then('see selected car successfully displayed on the page', () => {
    cy.url().should('include', 'autos');
    AllCarsPage.verifyFilteredCarText.contains("Wir haben")
})

