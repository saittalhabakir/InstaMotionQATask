import {Given, When, Then, And} from "cypress-cucumber-preprocessor/steps";
import {AllCarsPage} from "../../Pages/AllCarsPage";
import {FavoritesListPage} from "../../Pages/FavoritesListPage";

Given('choose more than once car to add favorite list', () => {
    AllCarsPage.chooseFirstCarToFavoritesDiv().click();
    cy.scrollTo('bottom')
    AllCarsPage.chooseLastCarToFavoritesDiv.click();
    AllCarsPage.favoriteListPageBtn.click()
    cy.url().should('include', 'favoriten');
})

Then('verify cars successfully displayed on the favorites page', () => {
    FavoritesListPage.carAddedFavoriteListImages().should("be.visible").and("have.value", "€/Monat");
})