Feature: Add Car Favorite List

    I want to add car to favorite list

  Scenario : As a user, I should be able to add car to favorite list successfully
    Given clicks on all cars button and verify all cars page url
    When  choose more than once car to add favorite list
    Then  verify cars successfully displayed on the favorites page