import {HomePage} from "../Pages/HomePage";
import {Given} from "cypress-cucumber-preprocessor/steps";

before(() => {
  cy.openHomePage()
  HomePage.acceptCookiesBtn.click()
  cy.wait(3000)
});

Given("clicks on all cars button and verify all cars page url", () => {
  HomePage.allCarsBtn.click()
  cy.url().should('include', 'autos');
})
