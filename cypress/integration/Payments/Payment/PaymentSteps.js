import {And, Given, When, Then} from "cypress-cucumber-preprocessor/steps";
import {AllCarsPage} from "../../Pages/AllCarsPage";
import {PaymentPage} from "../../Pages/PaymentPage";
import {HomePage} from "../../Pages/HomePage";

Given('clicks on any car on the list', () => {
    AllCarsPage.chooseAnyCar.any().click()
    cy.url().should('include', 'auto');
})

When('clicks on start payment button', () => {
    PaymentPage.startPaymentBtn.click()
})

And('choose online transfer purchase payment option and click on continue button', () => {
    PaymentPage.transferOnlinePaymentBtn.click()
    PaymentPage.continueBtn.click()
})

And('user fill in the required fields', () => {
    PaymentPage.enterPhoneNumberTextArea.type("1781445858")
    PaymentPage.enterNameTextArea.type("Sait")
    PaymentPage.enterSurnameTextArea.type("Bakir")
    PaymentPage.enterMailTextArea.type("abc@gmail.com")
    PaymentPage.tradeInYourCarSelect.select('Ja').should('have.value', 'Ja')
})

And('clicks on terms of privacy policy button and click on continue button', () => {
    PaymentPage.termsOfThePrivacyAndNewInformationBtn.click({multiple: true})
    PaymentPage.sendBtn.click()
})

Then('see purchase order successfully send', () => {
    PaymentPage.verifyPurchaseOrderSuccessfullySendMessage.contains("Deine Nachricht wurde erfolgreich gesendet")
})