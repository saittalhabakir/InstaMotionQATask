Feature: Car Payment Functionality

      I want to make the purchase

  Scenario: As a user, I should be able to purchase selected car
    Given clicks on all cars button and verify all cars page url
    When  clicks on any car on the list
    And   clicks on start payment button
    And   choose online transfer purchase payment option and click on continue button
    And   user fill in the required fields
    And   clicks on terms of privacy policy button and click on continue button
    Then  see purchase order successfully send