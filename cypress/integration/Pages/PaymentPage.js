export class PaymentPage {

    static get startPaymentBtn() {
        return cy.get("button.sc-dnqmqq.eDqzGW")
    }

    static get transferOnlinePaymentBtn() {
        return cy.get('.sc-hgRTRy > :nth-child(2)')
    }

    static get continueBtn() {
        return cy.get("div.options__Column-oxhfkq-3.options__LeftColumn-oxhfkq-4.ZSoyI > button")
    }

    static get sendBtn() {
        return cy.get('.ModalLead__Wrapper-sc-1x4hd3r-0 > .sc-dnqmqq')
    }

    static get enterPhoneNumberTextArea() {
        return cy.get("input[placeholder='Telefonnummer']")
    }

    static get enterNameTextArea() {
        return cy.get("input[placeholder='Gib Deinen Vornamen ein']")
    }

    static get enterSurnameTextArea() {
        return cy.get("input[placeholder='Gib Deinen Nachnamen ein']")
    }

    static get enterMailTextArea() {
        return cy.get("input[placeholder='Gib Deine E-Mail Adresse ein']")
    }

    static get tradeInYourCarSelect() {
        return cy.get(':nth-child(11) > :nth-child(1) > .sc-jnlKLf > .sc-bbmXgH > .sc-gGBfsJ')
    }

    static get termsOfThePrivacyAndNewInformationBtn() {
        return cy.get("div[class='sc-jlyJG BtHGL']")
    }

    static get verifyPurchaseOrderSuccessfullySendMessage() {
        return cy.get(".sc-dymIpo")
    }
}

