export class AllCarsPage {

    static get chooseAnyCar() {
        return cy.get(".sc-hdPSEv")
    }

    static get verifySelectedCarDiv() {
        return cy.get("h3[property='name']").first()
    }

    static get chooseFirstCarToFavoritesDiv() {
        return cy.get('.sc-gleUXh').first()
    }

    static get chooseLastCarToFavoritesDiv() {
        return cy.get('.sc-gleUXh').last()
    }

    static get verifyFilteredCarText() {
        return cy.get('.sc-gGCbJM')
    }

    static get favoriteListPageBtn() {
        return cy.get("a.sc-cjHlYL.gZHuqi:visible")
    }
}