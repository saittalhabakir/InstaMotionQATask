export class HomePage {

    static get acceptCookiesBtn() {
        return cy.get('#onetrust-accept-btn-handler')
    }

    static get allCarsBtn() {
        return cy.get("a[href='/autos']:visible")
    }

    static get brandNameDiv() {
        return cy.get('[data-testid="landing-make-selector"]')
    }

    static get brandListDiv() {
        return cy.get('#downshift-338194-menu:visible')
    }

    static get modalNameDiv() {
        return cy.get('[data-testid="landing-model-selector"]')
    }

    static get modalListDiv() {
        return cy.get("#downshift-338195-menu:visible")
    }

    static get searchBtn() {
      return cy.get("a[data-testid='landing-search-button']")
    }
}