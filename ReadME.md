# README
Hi!
this is the readme file of the *+InstaMotion QA Automation Engineer Task - test automation challenge.*  I have automated the InstaMotion pages as UI.
## Overview
This project is designed for UI automation testing. I used JavaScript as a programming language, Cypress as a Testing Framework, Cucumber BDD, Gherkin language, Mochawesome reporting. All the details of the Framework will be explained below.
## Short Intro and Benefits
I can describe my framework as **easy to maintain, scalable and highly reusable** because of the following reasons:
- I am using **Page Object Model**, which separates test scripts and web element locators. By that way even if there is a change in a web element which is used in many places, I am changing it in a central one place.
- It has **highly reusable** because of Cucumber scenario methods. The same method can be used for many test cases. Write once and use it many times. (see my step definitions and feature files.)
- I am separating locators, data, features and test scripts. Hence it is very **scalable**. The framework that I prepared can be used even thousands of data or test cases.
## Install to Local and Run
Its a **node.js** project. Therefore you need to pull it to your machine and run either through terminal. After open the Cypress and locating terminal to the project directory, type the following:
`npm run test`
## Framework and Patterns
In a node.js project I have the following structure:
### Root Files
- **node_modules file:** to manage libraries, and plugins.
### Library
- **pages package:** this package contains a lot of pages for the *whole application* which contains the ***Page Object Model Design Pattern***,  common menus and web elements. Moreover, the package contains Page class for every page in the web app, which centralizes the locatorss and methods related to the page.
- **common package:** this package contains Hooks classes which uses setup and teardown of the environment before and after each scenario.
- **step definitions class:** those classes contains the test script methods. Test scripts usess objects created from the page classes.
- **Gherkin files:** I save my cucumber feature file. By doing that I'm make sure that my test cases are understandable for each team member of, even for those who are not technically strong.
- **cypress.json file** this files used to store any configuration values you supply.
- **package.json file:** this file records important metadata about a project which is required before publishing to NPM.

# Final Say
I hope this task has shown my interest in InstaMotion and the opportunities given by them and my desire to learn and develop more.
